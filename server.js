const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

let app = express();
let port = process.env.PORT || 8080;

//body Parser
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())
//Pug Load view engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res){
  res.render('index',{title: "Socket IO"});
})

var server = require('http').Server(app);
var io = require('socket.io')(server);
server.listen(port, function(){
  console.log('server is running on port: '+ port);
});
//const io = require('socket.io').listen(4000).sockets;
let clients = [];
io.on('connection', function(socket){
  console.log(socket.id +" has connected "+clients.length);
  clients.forEach(function(client){
    socket.emit('activeMouse',client.id);
  });
  clients.push(socket);
  broadcast('activeMouse',socket.id);
  socket.on('disconnect', function(){
    console.log(socket.id +" has disconnected");
    broadcast('removeMouse',socket.id);
    var index;
    for (var index = 0; index < clients.length; index++) {
      if(clients[index].id == socket.id) break;
    }
    clients.splice(index,1);
  })
  socket.on('mousepos', function(pos){
    //console.log(socket.id+" move mouse> x:" +pos.x+" y:"+pos.y);
    let moveMouse = {
      id : socket.id,
      pos: pos
    }
    broadcast('mousemove',moveMouse);
  })
  socket.on('msg', function(data){
    clients.forEach(function(client){
      client.emit('msg',"< " +socket.id+" >");
      client.emit('msg',data);
    })
  })

  function broadcast(tag, msg){
    clients.forEach(function(client){
      if(client!=socket){
        client.emit(tag,msg);
      }
    });
  }
})
