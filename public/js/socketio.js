$(document).ready(function(){
  console.log("Document is ready");
  var socket = io.connect({
    reconnection: true,
    reconnectionDelay: 1000,
    reconnectionDelayMax : 5000,
    reconnectionAttempts: 99999
});
  socket.on('connect', function(){
    console.log('connected to server');
  });
  socket.on('event', function(data){});
  socket.on('disconnect', function(){
    socket.emit('disconnect');
    console.log('disconnect');
  });
  $('#send-btn').click(function(){
    socket.emit('msg',$('#msg-input').val());
    $('#msg-input').val('');
  });
  $(document).keypress(function(e){
    if(e.which == 13){
      socket.emit('msg',$('#msg-input').val());
      $('#msg-input').val('');
  }});

  socket.on('msg', function(data){
    console.log(data);
    $('#chatcontent').append('<li class="list-group-item">'+data+'</li>')
  });
  socket.on('activeMouse', function(id){
    $('#cursorList').append('<div class="cursor" id="'+id+'mouse"></div>');
  });
  socket.on('mousemove',function(movepos){
    let realX =movepos.pos.x*$(document).width();
    let realY =movepos.pos.y*$(document).height();
    if(realX+20<$(document).width())
      $('#'+movepos.id+'mouse').css("left",realX);
    if(realY+20<$(document).height())
      $('#'+movepos.id+'mouse').css("top",realY);
  });
  socket.on('removeMouse', function(id){
    $('#'+id+'mouse').remove();
  })
  $(document).mousemove(function(e){
    var scrollY = $(window).scrollTop();
    var scrollX = $(window).scrollLeft();
    var posX = (e.pageX - scrollX -10)/$(document).width();
    var posY = (e.pageY - scrollY -10)/$(document).height();
    socket.emit('mousepos',{x: posX, y: posY});
  });
})
